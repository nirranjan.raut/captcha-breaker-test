package com.example.captcha.breaker.captchabreakertest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UIController {

    @GetMapping(path = "/")
    public String home() {
        return "index";
    }

}
