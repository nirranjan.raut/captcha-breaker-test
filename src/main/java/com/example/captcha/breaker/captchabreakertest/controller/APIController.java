package com.example.captcha.breaker.captchabreakertest.controller;

import com.example.captcha.breaker.captchabreakertest.service.WebCrawlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class APIController {
    @Autowired
    private WebCrawlerService service;

    @GetMapping(path = "/start")
    public Map<String, String> breakCaptcha() {
        Map<String, String> map = new HashMap<>();
        service.doSomethingUseful();
        map.put("status", "success");
        return map;
    }

    @GetMapping(path = "/break")
    public Map<String, String> startManual() {
        Map<String, String> map = new HashMap<>();
//        service.doSomethingElse();
        map.put("status", "success");
        return map;
    }

}
