package com.example.captcha.breaker.captchabreakertest.service;


import com.fast.captcha.CaptchaSolver;
import com.fast.captcha.model.FastCaptchaResponse;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class WebCrawlerService {

    @Value("${web.url}")
    private String webUrl;

    @Value("${api.secret.key}")
    private String apiSecretKey;

    private static final Logger LOGGER = LoggerFactory.getLogger(WebCrawlerService.class.getName());

    @Async
    public void doSomethingUseful() {
        WebDriver chromeDriver = null;
        try {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--remote-allow-origins=*");
            chromeDriver = new ChromeDriver(options);
            chromeDriver.manage().window().maximize();
            //url for which you want to solve the captcha
            //in my case webUrl is https://www.google.com/recaptcha/api2/demo
            chromeDriver.get(webUrl);
            //data-sitekey of that website
            //TODO update data-sitekey based on your web.url
            // if you don't know what is data-sitekey, then watch my youtube video here https://youtu.be/OdpgfFKy0qA
            String websiteKey = "6Le-wvkSAAAAAPBMRTvw0Q4Muexq9bi0DJwx_mJ-";
            //get the balance of fast-captcha service account
            FastCaptchaResponse balanceResponse = CaptchaSolver.balance(apiSecretKey);
            LOGGER.info("Balance is {}", balanceResponse.getBalance());
            //solve the captcha
            FastCaptchaResponse recaptchaResponse = CaptchaSolver.solve(apiSecretKey, websiteKey, webUrl);
            if(!"SUCCESS".equals(recaptchaResponse.getStatus())) {
                LOGGER.error("Response is not successful {}", recaptchaResponse.getMessage());
                throw new RuntimeException("Captcha solution not found.");
            }
            String solution = recaptchaResponse.getSolution();
            //enter it in text area
            WebElement captchaSolutionElement = chromeDriver.findElement(By.id("g-recaptcha-response"));
            String javaScript = "arguments[0].style.height = 'auto'; arguments[0].style.display = 'block';";
            ((JavascriptExecutor) chromeDriver).executeScript(javaScript, captchaSolutionElement);
            captchaSolutionElement.sendKeys(solution);
            Thread.sleep(500);

            //finally submit the form
            WebElement buttonSubmit = chromeDriver.findElement(By.cssSelector("input[type=submit]"));
            buttonSubmit.click();
            //wait to see the result before closing chrome
            Thread.sleep(5 * 1000);
        } catch (Exception ex) {
            LOGGER.error("Exception in submitting the page. {}", ex.getMessage(), ex);
        } finally {
            //close the chrome
            if(chromeDriver != null) {
                chromeDriver.quit();
            }
        }
    }

}
